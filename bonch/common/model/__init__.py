from bonch.common.model.SubjectType import SubjectType

subject_type_map = {
    "Лекция": SubjectType.LECTURE,
    "Зачет": SubjectType.TEST,
    "Экзамен": SubjectType.EXAM,
    "Практические занятия": SubjectType.PRACTICAL_LESSON,
    "Лабораторная работа": SubjectType.LABORATORY_CLASSE
}
