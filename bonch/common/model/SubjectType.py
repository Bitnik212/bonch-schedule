from enum import Enum


class SubjectType(Enum):
    PRACTICAL_LESSON = "PRACTICAL_LESSON"
    LECTURE = "LECTURE"
    LABORATORY_CLASSE = "LABORATORY_CLASSE"
    TEST = "TEST"
    EXAM = "EXAM"
    OTHER = "OTHER"
